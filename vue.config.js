// vue.config.js
module.exports = {
  devServer: {
    host: 'quasar.test',
    https: false,
    port: 80
  }
}
