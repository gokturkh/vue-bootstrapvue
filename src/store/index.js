import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: () => ({
    isLoggedIn: false,
    user: null
  }),
  mutations: {
    updateIsLoggedIn (state, isLoggedIn) {
      state.isLoggedIn = isLoggedIn
    },
    setUser (state, user) {
      state.user = user
    }
  },
  getters: {
    getIsLoggedIn (state) {
      return state.isLoggedIn
    },
    getUser (state) {
      return state.user
    }
  },
  actions: {
    loginLogout (context, isLoggedIn) {
      context.commit('updateIsLoggedIn', isLoggedIn)
    },
    setUser (context, user) {
      context.commit('setUser', user)
    },
    getUser (context) {
      context.commit('getUser')
    }
  },
  modules: {
  }
})
