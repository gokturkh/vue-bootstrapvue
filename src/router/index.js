import Vue from 'vue'
import VueRouter from 'vue-router'
import store from '../store'
// import Home from '../views/Home.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    component: () => import('../layouts/MainLayout.vue'),
    children: [
      {
        path: '',
        component: () => import('../views/Home.vue'),
        name: 'Index'
      }
    ],
    meta: {
      requiresAuth: true
    }
  },
  {
    path: '/about',
    component: () => import('../layouts/MainLayout.vue'),
    children: [
      {
        path: '',
        component: () => import('../views/About.vue'),
        name: 'About'
      }
    ],
    meta: {
      requiresAuth: true
    }
  },
  {
    path: '/login',
    component: () => import('../layouts/LoginLayout.vue'),
    children: [
      {
        path: '',
        component: () => import('../views/Login.vue'),
        name: 'Login'
      }
    ],
    meta: {
      hideForAuth: true
    }
  },
  {
    path: '*',
    component: () => import('../views/Error404.vue')
  }
]

const router = new VueRouter({
  scrollBehavior: () => ({ x: 0, y: 0 }),
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

router.beforeEach((to, from, next) => {
  if (to.matched.some(record => record.meta.requiresAuth)) {
    if (!store.getters.getIsLoggedIn) {
      next({ name: 'Login' })
    } else {
      next() // go to wherever I'm going
    }
  } else if (to.matched.some(record => record.meta.hideForAuth)) {
    if (store.getters.getIsLoggedIn) {
      next({ name: 'Index' })
    } else {
      next()
    }
  } else {
    next() // does not require auth, make sure to always call next()!
  }
})

export default router
